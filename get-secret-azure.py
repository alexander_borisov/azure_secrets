from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient

credential = DefaultAzureCredential()

vault = "https://develop1111.vault.azure.net"

secret_client = SecretClient(vault_url= vault, credential=credential)

secret_properties = secret_client.list_properties_of_secrets()

for secret_property in secret_properties:
    print(secret_property.name)
    secret = secret_client.get_secret(secret_property.name)
    print(secret.value)



